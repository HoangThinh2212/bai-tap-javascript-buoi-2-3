// Câu 1
function tinhLuong() {
  var luong1ngayEl = document.getElementById("luong1ngay").value*1;
  var songaylamEl = document.getElementById("songaylam").value*1;

  // var luong1ngayValue = luong1ngayEl.value;
  // var songaylamValue = songaylamEl.value;
  var resultValue;

  // resultValue = luong1ngayValue * songaylamValue;
  resultValue = luong1ngayEl * songaylamEl;
  document.getElementById("result1").value = resultValue;
}

// Câu 2
function trungBinhCong() {
  // Ép kiểu sang số
  var so1El = document.getElementById("sot1").value * 1;
  var so2El = document.getElementById("sot2").value * 1;
  var so3El = document.getElementById("sot3").value * 1;
  var so4El = document.getElementById("sot4").value * 1;
  var so5El = document.getElementById("sot5").value * 1;

  // Ép kiểu sang số
  /* so1El = Number(so1El);
  so2El = Number(so2El);
  so3El = Number(so3El);
  so4El = Number(so4El);
  so5El = Number(so5El); */

  // Tính trung bình cộng
  document.getElementById("result2").value =
    (so1El + so2El + so3El + so4El + so5El) / 5;
}

// Câu 3
function quyDoiTien() {
  var soTienEl = document.getElementById("sotiendoi").value;
  const giaMacDinh = 23500;
  var ketqua;

  // Ép kiểu sang số
  soTienEl = Number(soTienEl);

  // Tính
  ketqua = soTienEl * giaMacDinh;

  // Đổi kiểu hiển thị
  ketqua = new Intl.NumberFormat("vn-VN").format(ketqua);

  document.getElementById("result3").value = ketqua; // Có thể dùng hàm .toLocaleString() thay thế
}

// Câu 4
function tinhHCN() {
  var chieuDaiEl = document.getElementById("chieuDai").value;
  var chieuRongEl = document.getElementById("chieuRong").value;

  chieuDaiEl = Number(chieuDaiEl);
  chieuRongEl = Number(chieuRongEl);

  document.getElementById("result4").innerHTML =
    "Chu vi: " +
    (chieuDaiEl + chieuRongEl) * 2 +
    "; Diện tích: " +
    chieuDaiEl * chieuRongEl;

  /* document.getElementById("resultChuVi").value = (chieuDaiEl + chieuRongEl) * 2;

  document.getElementById("resultDienTich").value = chieuDaiEl * chieuRongEl; */
}

// Câu 5
function tong_2_chu_so() {
  var inputEl = document.getElementById("digitNumber").value;
  var inputDonViEl, inputHangChucEl;

  // Ép kiểu sang Number
  inputEl = Number(inputEl);

  // Lấy số hàng đơn vị và số hàng chục
  inputDonViEl = inputEl % 10;
  inputHangChucEl = inputEl / 10;

  // dùng hàm floor để làm tròn số hàng chục sau khi chia cho 10 -- nghĩa là số hàng chục không còn là số thập phân nữa
  inputHangChucEl = Math.floor(inputHangChucEl);

  // Cộng lại số hàng đơn vị và số hàng chục
  inputEl = inputDonViEl + inputHangChucEl;

  document.getElementById("result5").innerHTML = inputEl;
}
